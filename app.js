const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
require('dotenv').config();

const indexRouter = require('./routes/index');
const productsRouter = require('./routes/products');

const passportJWT = require('./middlewares/passport-jwt');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/v1', indexRouter); // http://localhost:4000/api/v1
app.use('/api/v1/products', [passportJWT.isLogin] ,productsRouter); // http://localhost:4000/api/v1/products

module.exports = app;
