const express = require('express');
const router = express.Router();

const model = require('../models/index');
const connectRabbitMQ = require('../config/rabbitmq');

/* /api/v1/products/ */
router.get('/', async function(req, res, next) {
  const products = await model.Product.findAll();

  const totalProducts = await model.Product.count();

  return res.status(200).json({
    total: totalProducts,
    data: products
  });
});

/* /api/v1/products/create */
router.post('/create', async function(req, res, next) {
  const {name, price} = req.body;

  //บันทึกลงตาราง products
  const newProduct = await model.Product.create({
    name: name,
    price: price,
  });

  //ส่งสินค้าที่เพิ่ม ไปที่ order-report service by rabbitmq
  const channel = await connectRabbitMQ();
  await channel.assertExchange('ex.akenarin', 'direct', { durable: true });
  await channel.assertQueue('q.akenarin.product.created', {durable: true});
  await channel.bindQueue('q.akenarin.product.created', 'ex.akenarin', 'rk.akenarin.product.created');
  channel.publish('ex.akenarin', 'rk.akenarin.product.created', Buffer.from(JSON.stringify(newProduct)) ,{
    contentType: 'application/json',
    contentEncoding: 'utf-8',
    persistent: true
  });

  return res.status(201).json({
    message: 'สร้างสินค้าใหม่สำเร็จ',
    product: {
      id: newProduct.id,
      name: newProduct.name,
      price: newProduct.price
    },
  });
});


module.exports = router;
